import axios from 'axios'
import urls from '@/lib/apiEndpoints';

const endpoint = "http://localhost:8000/api/v1";

class QM {
    constructor() {
        let headers = {};
        let token = localStorage.getItem('auth_token');
        if (token) {
            headers['Authorization'] = "Token " + token;
        }

        this.instance = axios.create({
            headers: {
                post: headers,
                get: headers
            }
        });
    }

    get(url, params = {}) {
        let path = endpoint + this.findPath(url);

        if (params) {
            path += '?' + Object.keys(params)
                .map(key => {
                    if (Array.isArray(params[key])) {
                        let q = '';
                        for (let i in params[key]) {
                            q += `${encodeURIComponent(key)}=${encodeURIComponent(params[key][i])}&`;
                        }
                        q = q.trim('&');

                        return q;
                    }
                    else return `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`
                })
                .join('&');
        }

        return this.instance.get(path)
    }

    post(url, params = {}) {
        let path = endpoint + this.findPath(url);
        return this.instance.post(path, params)
    }

    findPath(path) {
        if (typeof path == 'object') {
            if (urls[path.name] != undefined) return urls[path.name] + path.param + '/';
        }
        else {
            if (urls[path] != undefined) return urls[path];
            else return path;
        }
    }
}

window.QM = new QM();
