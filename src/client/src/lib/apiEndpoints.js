const urls = {
    'login': '/user/login',
    'user-details': '/user/details',
    'store': '/store/',
    'product-detail': '/product/',
};

export default urls;