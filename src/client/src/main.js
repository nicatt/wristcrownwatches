import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router'
import store from './store'
import "@storefront-ui/vue/styles.scss";
import './plugins/element.js'
import './plugins/qm.js'

Vue.use(Vuelidate)

Vue.config.productionTip = false

window.EventBus = new Vue();

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
