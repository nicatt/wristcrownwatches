import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loadingState: false,
        user: {
            isLoggedIn: false
        },
        waitMe: false, // used for first impression (checking token)
        basket: JSON.parse(localStorage.getItem('basket')),
        wishList: JSON.parse(localStorage.getItem('wishList')),
    },
    mutations: {
        LOGIN: (state, data) => {
            state.user.isLoggedIn = true;

            state.user = {
                ...state.user,
                ...data
            }
        },
        SET_LOADING: (state, data) => {
            state.loadingState = data
        },
        SET_WAITME: (state, data) => {
            state.waitMe = data;

            if (!data) window.EventBus.$emit('WAIT_ME', false);
        },
        SET_BASKET: (state, data) => {
            state.basket = data;
        },
        SET_WISH_LIST: (state, data) => {
            state.wishList = data;
        }
    },
    actions: {
        login({commit}, data) {
            localStorage.setItem('auth_token', data.token);

            commit('LOGIN', data);
        },
        addToBasket({commit}, {id, count}) {
            let basket = localStorage.getItem('basket');

            if (basket) {
                basket = JSON.parse(basket);
                basket[id] = basket[id] && basket[id] < count ? basket[id] + count : count;

                localStorage.setItem('basket', JSON.stringify(basket));
            } else {
                localStorage.setItem('basket', JSON.stringify({
                    [id]: count
                }));
            }

            commit('SET_BASKET', JSON.parse(localStorage.getItem('basket')));
        },
        updateBasketQuantity({commit, getters}, {id, qty}) {
            if (getters.basket[id]) {
                let basket = getters.basket;
                basket[id] = qty;

                localStorage.setItem('basket', JSON.stringify(basket));
                commit('SET_BASKET', JSON.parse(localStorage.getItem('basket')));

            }
        },
        removeFromBasket({commit, getters}, id) {
            if (getters.basket[id]) {
                let basket = getters.basket;
                delete basket[id];

                localStorage.setItem('basket', JSON.stringify(basket));
                commit('SET_BASKET', JSON.parse(localStorage.getItem('basket')));
            }
        },
        addToWishList({commit}, id) {
            let wish = localStorage.getItem('wishList');

            if (wish) {
                wish = JSON.parse(wish);
                if (!wish.includes(id)) wish.push(id);

                localStorage.setItem('wishList', JSON.stringify(wish));
            } else localStorage.setItem('wishList', JSON.stringify([id]));

            commit('SET_WISH_LIST', JSON.parse(localStorage.getItem('wishList')));
        },
        removeItemWishList({commit, getters}, id) {
            let wishList = getters.wishList.filter(w => w != id);

            localStorage.setItem('wishList', JSON.stringify(wishList));

            commit('SET_WISH_LIST', JSON.parse(localStorage.getItem('wishList')));
        }
    },
    getters: {
        user: state => state.user,
        isUserAnonymous: state => !state.user.token,
        loadingState: state => state.loadingState,
        waitMe: state => state.waitMe,
        basket: state => state.basket,
        wishList: state => state.wishList,
        basketCount: state => state.basket ? Object.keys(state.basket).length : 0,
        wishListCount: state => state.wishList ? state.wishList.length : 0,
    }
})
