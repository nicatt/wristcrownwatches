import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Browse from '@/views/Browse.vue'
import Cart from '@/views/Cart.vue'
import Favorites from '@/views/Favorites.vue'
import Product from '@/views/Product.vue'
import Account from '@/views/Account.vue'
import Login from '@/views/Login.vue'
import NotFound from '@/views/NotFound.vue'
import NProgress from 'nprogress'

import 'nprogress/nprogress.css';

Vue.use(VueRouter)

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/browse/men',
    alias:  '/browse/women',
    name: 'browse',
    component: Browse
  },
  {
    path: '/cart',
    name: 'cart',
    component: Cart
  },
  {
    path: '/favorites',
    name: 'favorites',
    component: Favorites
  },
  {
    path: '/product/:id',
    name: 'product',
    component: Product
  },
  {
    path: '/user/login',
    name: 'login',
    component: Login
  },
  {
    path: '/user/account',
    name: 'account',
    component: Account
  },
  {
    path: '*',
    alias: 'not-found',
    name: 'not-found',
    component: NotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// eslint-disable-next-line
router.beforeResolve((to, from, next) => {
  // If this isn't an initial page load.
  if (to.name) {
    // Start the route progress bar.
    NProgress.start()
  }
  next()
})
// eslint-disable-next-line
router.afterEach((to, from) => {
  // Complete the animation of the route progress bar.
  NProgress.done()
})

export default router
