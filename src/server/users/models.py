from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from .manager import UserManager


class User(AbstractUser):
    username = None
    email = models.EmailField(_("Email address"), unique=True, error_messages={
        'unique': _("A user with that email already exists."),
    })

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()
