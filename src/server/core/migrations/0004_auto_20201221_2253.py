# Generated by Django 3.1.4 on 2020-12-21 18:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_product_specification'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='old_price',
            field=models.DecimalField(decimal_places=2, default=0, help_text='Leave it blank if there is no discount', max_digits=7),
        ),
    ]
