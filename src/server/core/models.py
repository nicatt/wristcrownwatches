from decimal import Decimal

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models


class Filter(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Specification(models.Model):
    name = models.CharField(max_length=100)
    filter = models.ForeignKey(Filter, related_name='specs', on_delete=models.CASCADE)

    @property
    def filter_name(self):
        return self.filter.name

    def __str__(self):
        return f"{self.filter.name} - {self.name}"


class Product(models.Model):
    name = models.CharField(max_length=150)
    price = models.DecimalField(max_digits=7, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])
    old_price = models.DecimalField(max_digits=7, decimal_places=2, default=0,
                                    # validators=[MinValueValidator(Decimal('0.01'))],
                                    help_text="Leave it blank if there is no discount")
    description = models.TextField(max_length=1000, default=None, null=True, blank=True)
    specifications = models.ManyToManyField(Specification)

    @property
    def thumbnail(self):
        try:
            return self.images.get(is_cover=True).path
        except ObjectDoesNotExist:
            return self.images.first().path

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name

    def clean(self):
        if 0 < self.old_price <= self.price:
            raise ValidationError({
                'old_price': "Old price must be bigger than the current price"
            })


class Image(models.Model):
    product = models.ForeignKey(Product, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(upload_to='product/images/%Y/%m/%d/')
    is_cover = models.BooleanField(default=False)

    @property
    def path(self):
        return settings.CDN_DOMAIN + settings.MEDIA_URL + self.image.name
