from django.contrib import admin
from .models import Specification, Filter, Product, Image

admin.site.register(Filter)
admin.site.register(Specification)
admin.site.register(Product)
admin.site.register(Image)
