from django.urls import path
from rest_framework import routers

from . import views

router = routers.SimpleRouter()
router.register('product', views.ProductViewSet, basename="product")

app_name = 'api'

urlpatterns = [
    path('user/login', views.LoginApiView.as_view(), name="login"),
    path('user/details', views.UserDetailsApiView.as_view(), name="user-details"),
    # path('filters/', views.FilterApiView.as_view(), name="filters"),
    path('store/', views.StoreApiView.as_view(), name="store"),
]

urlpatterns += router.urls
