from django.contrib.auth import authenticate, get_user_model
from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _

User = get_user_model()


class DetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'email',
            'first_name',
            'last_name',
        )
        read_only_fields = (
            'email',
            'first_name',
            'last_name',
        )


class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        user = authenticate(**attrs)

        if user and user.is_active:
            return user

        raise serializers.ValidationError(_('Bad credentials. Unable to login.'))
