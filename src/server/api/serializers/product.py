from rest_framework import serializers

from core.models import Product, Specification, Filter


class SpecificationSerializer(serializers.ModelSerializer):
    filter_id = serializers.SerializerMethodField('get_filter_id')

    class Meta:
        model = Specification
        fields = ('id', 'name', 'filter_id', 'filter_name')

    def get_filter_id(self, obj):
        return obj.filter.pk


class ProductListSerializer(serializers.ModelSerializer):
    """Used for browse page"""

    price = serializers.SerializerMethodField()
    old_price = serializers.SerializerMethodField()
    specifications = SpecificationSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = (
            'id',
            'name',
            'price',
            'old_price',
            'thumbnail',
            'specifications',
        )

    def get_price(self, obj):
        return f'${obj.price}'

    def get_old_price(self, obj):
        return f'${obj.old_price}' if obj.old_price > 0 else None


class ProductSerializer(serializers.ModelSerializer):
    price = serializers.SerializerMethodField()
    old_price = serializers.SerializerMethodField()
    specifications = SpecificationSerializer(read_only=True, many=True)
    images = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = (
            'name',
            'price',
            'old_price',
            'description',
            'specifications',
            'images',
        )

    def get_price(self, obj):
        return f'${obj.price}'

    def get_old_price(self, obj):
        return f'${obj.old_price}' if obj.old_price > 0 else None

    def get_images(self, obj):
        return [{'is_cover': i.is_cover, 'path': i.path} for i in obj.images.all()]


class FilterSerializer(serializers.ModelSerializer):
    specifications = serializers.SerializerMethodField()

    class Meta:
        model = Filter
        fields = ('id', 'name', 'specifications')

    def get_specifications(self, obj):
        specs = Specification.objects.filter(filter=obj)
        return SpecificationSerializer(specs, many=True, read_only=True).data
