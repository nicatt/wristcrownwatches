from django.db.models import Count, Q
from rest_framework import generics, permissions, viewsets, mixins
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from core.models import Product
from .serializers.product import ProductSerializer, ProductListSerializer
from .serializers.user import LoginSerializer, DetailsSerializer
from .utils.pagination import DefaultPagination


class LoginApiView(generics.GenericAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = LoginSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data

        response = DetailsSerializer(user).data
        response['token'] = self.update_token(user)

        return Response(response)

    def update_token(self, user):
        Token.objects.get(user=user).delete()
        token = Token.objects.create(user=user)

        return token.key


class UserDetailsApiView(APIView):
    def post(self, request):
        response = DetailsSerializer(request.user).data

        return Response(response)


# class ProductViewSet(viewsets.ReadOnlyModelViewSet):
#     permission_classes = [permissions.AllowAny]
#     pagination_class = DefaultPagination
#     pagination_class.page_size = 10
#     serializer_class = ProductSerializer
#
#     def get_queryset(self):
#         params = self.request.GET.getlist('specs')
#         if params is None or params == []:
#             return Product.objects.all()
#         else:
#             return Product.objects.filter(
#                 specifications__in=params,
#             ).exclude(
#                 # Exclude any that aren't in params
#                 ~Q(specifications__in=params)
#             ).annotate(
#                 matches=Count('specifications', distinct=True)
#             ).filter(
#                 # Make sure the number found is right.
#                 matches=len(params)
#             )


# class FilterApiView(generics.ListAPIView):
#     serializer_class = FilterSerializer
#     permission_classes = [permissions.AllowAny]
#     queryset = Filter.objects.all()


class StoreApiView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request):
        sorting_list = {
            'az': 'name',
            'za': '-name',
            '19': 'price',
            '91': '-price',
        }
        sort = self.request.GET.get('sort')
        sort = sorting_list[sort] if sorting_list.get(sort) else sorting_list['az']

        specs = self.request.GET.getlist('specs')
        if specs is None or specs == []:
            all_products = Product.objects.all().order_by(sort)
        else:
            all_products = Product.objects.filter(
                specifications__in=specs,
            ).exclude(
                # Exclude any that aren't in specs
                ~Q(specifications__in=specs)
            ).annotate(
                matches=Count('specifications', distinct=True)
            ).filter(
                # Make sure the number found is right.
                matches=len(specs)
            ).order_by(sort)

        all_filters = {}
        for product in all_products:
            for spec in product.specifications.all():
                if spec.filter_id not in all_filters:
                    all_filters[spec.filter_id] = {
                        'name': spec.filter.name,
                        'specifications': {
                            spec.id: spec.name
                        }
                    }
                else:
                    all_filters[spec.filter_id]['specifications'][spec.id] = spec.name

        paginator = DefaultPagination()
        piece = paginator.paginate_queryset(request=request, queryset=all_products)
        products = ProductListSerializer(piece, many=True).data

        return paginator.get_paginated_response({
            'filters': all_filters,
            'products': products
        })


class ProductViewSet(GenericViewSet, mixins.RetrieveModelMixin):
    permission_classes = (permissions.AllowAny,)
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
