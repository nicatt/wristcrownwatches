#!/bin/bash

MANAGE_PY='./src/server/manage.py'

# Collect static files
#echo "Collecting static files..."
#python3 ${MANAGE_PY} collectstatic --noinput

# Make migrations
echo "Making migrations..."
python3 ${MANAGE_PY} makemigrations

# Apply migrations
echo "Applying migrations..."
python3 ${MANAGE_PY} migrate

# Start app
echo "Starting the server..."
python3 ${MANAGE_PY} runserver 0.0.0.0:8000
